# Test to pick up something moving in a set of 3 files
# written for base exposures

from photutils import DAOStarFinder
from astropy.io import fits
from astropy.stats import sigma_clipped_stats

from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize

import matplotlib.pyplot as plt
from astropy.visualization import SqrtStretch, ZScaleInterval
from astropy.visualization.mpl_normalize import ImageNormalize
from photutils import CircularAperture

from matplotlib.patches import Circle


ref_filename = '/home/amelia/Documents/summerresearch/asteroiddetection/test_data_157/clean_ref-gb3-R-2-0_1.fit'
# filanames, these should be registered to the reference image
files = [#'/home/amelia/Documents/summerresearch/asteroiddetection/test_data_157/d_B1809-gb3-R-2-0_1.fit',
         '/home/amelia/Documents/summerresearch/asteroiddetection/test_data_157/r_B1814-gb3-R-2-0_1.fit',
         '/home/amelia/Documents/summerresearch/asteroiddetection/test_data_157/r_B1819-gb3-R-2-0_1.fit',
         '/home/amelia/Documents/summerresearch/asteroiddetection/test_data_157/r_B1824-gb3-R-2-0_1.fit']

f = fits.open('/home/amelia/Documents/summerresearch/asteroiddetection/test_data_157/z_B1809-gb3-R-2-0_1.fit')
mask = (1 - f[0].data).astype(bool)
f.close()

jd_start = [#2453917.932985,
            2453917.969771, 2453918.077472, 2453918.117941
            ]

MATCH_RADIUS = 3
MAX_POS_MISMATCH = 2

PREDICT_RADIUS = 10

MAX_MAG_DIFFERENCE = 2

MAX_MAG = 0
# objevt list contains x,y, time, brightness


def get_objects(filename, ref_list=None, max_mag=MAX_MAG, threshold=5.,
                radius=MATCH_RADIUS):
    with fits.open(filename) as f:
        data = f[0].data
        mean, median, std = sigma_clipped_stats(data, sigma=3.0)

        daofind = DAOStarFinder(fwhm=2.0, threshold=threshold*std)

        # takes a zero point of zero
        # print(1 - mask)
        sources = daofind(data, mask=(1-mask).astype(bool))

        print(len(sources))
        if max_mag is not None:
            for row in sources[:]:
                if row['mag'] > max_mag:
                    sources.remove(row)

        # Convert to a dictionary for easier matching
        dict_rep = {}
        for row in sources:
            # Ig
            if max_mag is not None:
                if row['mag'] > max_mag:
                    continue

            coord = (int(row['xcentroid']/radius),
                     int(row['ycentroid']/radius))

            if ref_list and check_near(ref_list, coord,
                                       row['xcentroid'], row['ycentroid']):
                continue

            dict_rep[coord] = row

    return dict_rep


def check_near(ref_list, coord, xcentroid, ycentroid):
        if coord in ref_list:
            return True

        # check near by and take the quadure difference
        # see if that is greater than a certain value
        # check the four around
        # or check the 8 around?
        # eh, not much harder to check the 8
        d = [(0, 1), (1, 0), (0, -1), (-1, 0)]

        if ref_list:
            for x, y in d:
                coord2 = (coord[0] + x, coord[1] + y)
                if coord2 in ref_list:
                    y_ref = ref_list[coord2]['ycentroid']
                    x_ref = ref_list[coord2]['xcentroid']

                    t_ = ((ycentroid - y_ref)**2
                          + (xcentroid - x_ref)**2)
                    if t_ < MAX_POS_MISMATCH**2:
                        return True
        return False


def predict_object_positions(first_objects, second_objects, time1, time2):
    # time1 is time between first two exposures
    # time2 is time between second and third exposures

    new_positions = {}
    for coord, row in first_objects.items():
        if check_near(second_objects, coord,
                      row['xcentroid'], row['ycentroid']):
            print("Non moving object")
            continue
        for coord2, row2 in second_objects.items():
            if abs(row['mag'] - row2['mag']) > MAX_MAG_DIFFERENCE:
                # check size is of same magnitudeish
                print("Magnitude too different")
                continue

            # compute distance change
            # compute velocity
            # compute new position
            # check if that matches image boundraries
            # place in new list/dict

            dx = row2['xcentroid'] - row['xcentroid']
            dy = row2['ycentroid'] - row['ycentroid']

            x_new = row2['xcentroid'] + dx * time2/time1
            y_new = row2['ycentroid'] + dy * time2/time1
            if x_new < 0 or y_new < 0:
                print("off frame")
                continue

            key = (int(x_new/PREDICT_RADIUS), int(y_new/PREDICT_RADIUS))

            new_positions[key] = (row, row2, (x_new, y_new))

    return new_positions


def compare_predicted_with_actual_positions(predicted, actual):
    matches = []
    for coord, row in predicted.items():
        if coord in actual:
            first_object, second_object, predict_coord = row
            match_object = actual[coord]

            if (abs(first_object['mag'] - match_object['mag'])
                    > MAX_MAG_DIFFERENCE):

                print("Position match, magnitude different")
                continue

            matches.append((
                first_object, second_object, match_object, predict_coord))

    # there will be fewer actual hits (n) than predicted (n^2)
    # so compute the possible int xy values around the radius for the
    # actual values and see if there are any predicted hits
    # by checking the dictionary and then the magnitudes
    # then report this back in a list with each of the rows

    # do checking by condensing objects into a dict with rad 3
    # assume no to same objects close by rad 2
    return matches


def plot_match(match, files):
    fig, axs = plt.subplots(1, 3)
    # apertures = CircularAperture(positions, r=4.)
    # plt.imshow(data, cmap='Greys', origin='lower', norm=norm)

    circle_rad = 3
    for i in range(3):
        # Colors
        # Red initial object
        # Blue second object
        # green predicted object

        file = files[i]

        with fits.open(file) as f:
            data = f[0].data

            norm = ImageNormalize(data, interval=ZScaleInterval())
            axs[i].imshow(data, origin='lower', norm=norm)

            for x in range(3):
                m = match[x]
                if x == 0:
                    colour = (1, 0, 0)
                elif x == 1:
                    colour = (0, 0.5, 0)
                else:
                    colour = (0, 0, 1)

                circle = Circle((m['xcentroid'], m['ycentroid']), circle_rad,
                                fill=False, edgecolor=colour, linewidth=1)

                axs[i].add_patch(circle)

    plt.show()


def graph_objects(files, object_lists):
    l = len(files)
    fig, axs = plt.subplots(1, l)
    # apertures = CircularAperture(positions, r=4.)
    # plt.imshow(data, cmap='Greys', origin='lower', norm=norm)

    # overlay predicted positions
    circle_rad = 3
    for i in range(l):
        # Colors
        # Red initial object
        # Blue second object
        # green predicted object

        file = files[i]

        with fits.open(file) as f:
            data = f[0].data

            norm = ImageNormalize(data, interval=ZScaleInterval())
            axs[i].imshow(data, origin='lower', norm=norm)

            for _, ob in object_lists[i].items():
                circle = Circle((ob['xcentroid'], ob['ycentroid']), circle_rad,
                                fill=False, edgecolor=(1, 0, 0), linewidth=1)

                axs[i].add_patch(circle)

    plt.show()


# get reference stdh
ref_list = get_objects(ref_filename, threshold=5.)

initial_objects = get_objects(files[0], ref_list)
print(len(initial_objects))
second_objects = get_objects(files[1], ref_list)
third_objects = get_objects(files[2], ref_list, radius=PREDICT_RADIUS)
graph_objects(files, [initial_objects, second_objects, third_objects])


predicted_objects = predict_object_positions(
    initial_objects, second_objects,
    (jd_start[1] - jd_start[0]), (jd_start[2] - jd_start[1]))
print(len(predicted_objects))

# Graph predicted objects
matches = compare_predicted_with_actual_positions(predicted_objects,
                                                  third_objects)
# Plot matches

# fig, ax = plt.subplots()


print(matches)
print(len(matches))

for match in matches:
    plot_match(match, files)
# circle = mpatches.Circle(grid[0], 0.1, ec="none", fill=None)
# patches.append(circle)
# label(grid[0], "Circle")
# plt.imshow(data, cmap='Greys', origin='lower', norm=norm)

# compare third_objects and predicted_objects
# check for any matches and compare
