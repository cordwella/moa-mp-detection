"""
Utility functions for minor planet detection
"""

from astropy.visualization.mpl_normalize import ImageNormalize
from astropy.visualization import ZScaleInterval

import matplotlib.pyplot as plt
from matplotlib.patches import Circle

from mp_detection import constants


def graph_objects(exps):
    """
    Graphs all of the new objects found in a list of exposures passed to
    it.
    """

    n = len(exps)
    fig, axs = plt.subplots(1, n)
    circle_rad = 3
    for i in range(len(exps)):
        data = exps[i].image

        norm = ImageNormalize(data, interval=ZScaleInterval())
        axs[i].imshow(data, origin='lower', norm=norm)

        for _, ob in exps[i].objects.items():
            circle = Circle((ob['xcentroid'], ob['ycentroid']), circle_rad,
                            fill=False, edgecolor=(1, 0, 0), linewidth=1)
            axs[i].add_patch(circle)

    plt.show()


def check_near(ref_objects, coord, xcentroid, ycentroid):
    """ Check whether an object with xcentroid and y centroid
        is within range of an object inside ref_objects

        ref_objects is a dictionary with the shortened coordinates
        of the same form as coord with photutils table entries
        inside of it.

        coord is an (x, y) tuple of the truncated cooridinates for
        matching as generated in Exposure.get_objects

        xcentroid and ycentroid are floats representing the calculated
        centroid of the psf"""
    if coord in ref_objects:
        return True

    # check near by and take the quadure difference
    # see if that is greater than a certain value
    # check the 8 nearest points
    d = [(0, 1), (1, 0), (0, -1), (-1, 0), (1, -1), (-1, -1), (-1, 1)]

    if ref_objects:
        for x, y in d:
            coord2 = (coord[0] + x, coord[1] + y)
            if coord2 in ref_objects:
                y_ref = ref_objects[coord2]['ycentroid']
                x_ref = ref_objects[coord2]['xcentroid']

                t_ = ((ycentroid - y_ref)**2
                      + (xcentroid - x_ref)**2)
                if t_ < constants.MAX_POS_MISMATCH**2:
                    return True
    return False
