"""
Moving Object Detection

For single nights only, intention is if it is over muliple nights
intergrate forward and then compute and compare likely positions
(unlikely to be linear between nights)
"""

from astropy.io import fits
from astropy.stats import sigma_clipped_stats
from astropy.visualization.mpl_normalize import ImageNormalize
from astropy.visualization import ZScaleInterval

from enum import Enum
import imreg_dft as ird
import numpy as np
from photutils import DAOStarFinder
import os
import sys

from scipy.signal import convolve2d
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

from mp_detection.tracking.check_object import check_chip_field_for_asteroid
from mp_detection import constants
from mp_detection.util import check_near


class Exposure:
    """ Represents a single frame """
    def __init__(self, filename, reference=None, threshold=5.,
                 max_mag=constants.MAX_MAG, subf=(0, 0, 2048, 2048),
                 get_objects=True, redo_registration=False):
        self.filename = filename
        self.frame = filename.split("/")[-1][:-4]
        self.frame_id, self.field, self.colour, self.chip = self.frame.split("-")  # noqa

        self.reference = reference
        self.objects = None
        self.subf = subf

        self.registered_filename = "{}/reg-{}-{}.fit".format(
            constants.OUTPUT_DIR, self.frame, self.subf)

        if not redo_registration and os.path.isfile(self.registered_filename):
            print("Using existing registered file")
            # open registered frame
            with fits.open(self.registered_filename) as f:
                self.image = f[0].data
                self.date = f[0].header['JDMID']
        else:
            with fits.open(filename) as f:
                self.image = f[0].data[subf[0]:subf[2], subf[1]:subf[3]]
                if reference:
                    self.date = (
                        f[0].header['JDSTART'] + f[0].header['JDEND'])/2

            mean, median, std = sigma_clipped_stats(self.image, sigma=3.0)
            # self.image = self.image - median
            if reference:
                # use a fft to register the image against the refernce
                # image
                print("Registing file")
                result = ird.similarity(self.reference.image, self.image,
                                        numiter=3,
                                        constraints={"scale": [1, 0]})
                self.image = result['timg']

                # save registered frame
                if constants.SAVE_REGISTERED_FILES:
                    hdu = fits.PrimaryHDU(self.image)

                    hdu.header['SOURCEFN'] = self.filename
                    hdu.header['JDMID'] = self.date
                    hdu.writeto(self.registered_filename)

        self.mask = 1 - (self.compute_saturated_pixel_mask() *
                         self.compute_bleed_mask())
        self.xmax, self.ymax = self.image.shape

        if reference:
            self.mask = 1 - ((1 - self.mask) * (1 - reference.mask))

        if get_objects:
            self.get_objects()

    def compute_saturated_pixel_mask(self):
        # Taken from pyDIA
        # see notes on compute bleed mask for more details
        radius = constants.MASK_RADIUS
        rad2 = radius*radius
        rad = int(np.ceil(radius))
        z = np.arange(2*rad+1)-rad
        x, y = np.meshgrid(z, z)
        p = np.array(np.where(x**2 + y**2 < rad2))
        mask = np.ones(self.image.shape, dtype=bool)
        saturated_pixels = np.where(
            self.image > constants.IMAGE_SATURATION_VALUE)
        zp0 = z[p[0]]
        zp1 = z[p[1]]
        sp0 = saturated_pixels[0][:, np.newaxis]
        sp1 = saturated_pixels[1][:, np.newaxis]
        q0 = zp0 + sp0
        q1 = zp1 + sp1
        q0 = q0.flatten()
        q1 = q1.flatten()

        s = np.asarray(np.where((q0 >= 0) & (q0 < self.image.shape[0]) &
                                (q1 >= 0) & (q1 < self.image.shape[1])))[0]

        mask[q0[s], q1[s]] = 0
        return mask

    def compute_bleed_mask(self):
        # from pyDIA
        # NOTE(amelia): This is a modified version of some code from pyDIA
        # it works by first convolving the image and then does what seems
        # like a bunch of scary stuff to find all points to mask

        radius = 3
        kernel = np.array([[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                           [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                           [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]])
        rad2 = radius*radius
        d = self.image
        mask = np.ones_like(d, dtype=bool)
        # Perform convolution on the image with the kernel
        dc = convolve2d(d, kernel.T, mode='same')
        rad = int(np.ceil(radius))
        z = np.arange(2*rad+1)-rad
        x, y = np.meshgrid(z, z)
        p = np.array(np.where(x**2 + y**2 < rad2))

        # Saturated pixels and pixels causing bleed are at different
        # levels, the pixels are nonlinear for quite sometime before
        # bleed becomes an issue
        bad_pixels = np.where(
            np.abs(dc) > 1.4*constants.IMAGE_SATURATION_VALUE)
        zp0 = z[p[0]]
        zp1 = z[p[1]]
        # Zp0 and zp1 contain a grid of points such that when added to
        # any one point it gives an output of all of the points within rad
        # pixels
        sp0 = bad_pixels[0][:, np.newaxis]
        sp1 = bad_pixels[1][:, np.newaxis]
        q0 = zp0 + sp0
        q1 = zp1 + sp1
        q0 = q0.flatten()
        q1 = q1.flatten()
        # Add those to the output of bad pixels, ignore all points that are
        # outside the bounds of the image

        s = np.asarray(np.where((q0 >= 0) & (q0 < d.shape[0]) &
                                (q1 >= 0) & (q1 < d.shape[1])))[0]
        mask[q0[s], q1[s]] = 0

        # This code here is to check for bad columns, however this
        # doesn't really apply here with the setup saturation values
        # for i in range(mask.shape[1]):
        #    if np.sum(mask[:, i]) < 0.85*mask.shape[0]:
        #        mask[:, i] = 0
        return mask

    def clean_memory(self):
        self.image = None
        self.mask = None

    def get_objects(self, threshold=5.,
                    max_mag=constants.MAX_MAG):
        """Get all of the idenifiable new objects in the frame

        Inputs: threshold: thresholding for stellar identification in
                DAOStarFinder
                max_mag: Ignore stars above this instrumental magntiude

        Returns: self.objects
                A dictionary of detected objects that are not in the
                reference frame. The dictonary keys are the interger rounded
                versions of the pixel centroid divided by MATCH_RADIUSself.

                A similar dictionary (not returned) is placed into
                self.predict_objects with a larger spacing (PREDICT_RADIUS)
                to be used to check predictions against.
        """
        if self.objects:
            return self.objects
        if self.image is None:
            # TODO(amelia): get registered image here
            with fits.open(self.filename) as f:
                self.image = f[0].data

            if self.reference:
                result = ird.similarity(self.reference.image, self.image,
                                        numiter=3)
                self.image = result['timg']

        self.mask = self.mask.astype(bool)

        mean, median, std = sigma_clipped_stats(self.image, sigma=3.0)

        daofind = DAOStarFinder(fwhm=2.0, threshold=threshold*std)

        # takes a zero point of zero
        sources = daofind(self.image - median, mask=self.mask)

        if max_mag is not None:
            for row in sources[:]:
                if row['mag'] > max_mag:
                    sources.remove(row)

        # Convert to a dictionary for O(1) object matching
        dict_rep = {}
        predict_dict = {}

        for row in sources:
            if max_mag is not None:
                if row['mag'] > max_mag:
                    continue

            coord = (int(row['xcentroid']/constants.MATCH_RADIUS),
                     int(row['ycentroid']/constants.MATCH_RADIUS))

            if self.reference and check_near(self.reference.objects, coord,
                                             row['xcentroid'],
                                             row['ycentroid']):
                continue
            dict_rep[coord] = row

            # Larger spaced dictionary for checking predicions against
            coord2 = (int(row['xcentroid']/constants.PREDICT_RADIUS),
                      int(row['ycentroid']/constants.PREDICT_RADIUS))
            # TODO(amelia): See how nessecary/annoying it would be to have
            # multiple objects in one dictionary slot, current thoughts
            # are no not nessecary (and anyway if only one frame is missing
            # that shouldn't effect the overall result anyway)
            predict_dict[coord2] = row

        self.objects = dict_rep
        self.predict_objects = predict_dict
        return self.objects

    def __repr__(self):
        if reference:
            return ('<Exposure filename={filename}, '
                    'reference={reference}>'.format(
                        filename=self.filename, reference=reference.filename))
        else:
            return '<Exposure filename={filename}>'.format(
                filename=self.filename)


class ObjectState(Enum):
    """ Possible values for the state of a TrackedObject """
    ACTIVE = 1
    PREV_SATURATED = 2
    END_OFF_FRAME = 3
    LOST = 4
    LOST_MAGNITUDE_DIFFERENT = 5


class TrackedObject:
    """ Represents a detected moving object across
        all of the frames it is in """

    def __init__(self, instances, state=None):
        if state is None:
            # Is this object being actively tracked in the software
            # life cycle
            self.state = ObjectState.ACTIVE
        else:
            self.state = state

        # Instances are expected to be ordered by date
        # first in list was earliest observation
        # last in list is most recent observation
        # list of ObjectFrameInstance objects
        self.instances = instances

    # Short cut properties
    @property
    def long(self):
        return len(self.instances) > 3

    @property
    def last_instance(self):
        return self.instances[-1]

    @property
    def first_instance(self):
        return self.instances[0]

    @property
    def active(self):
        return (self.state is ObjectState.ACTIVE or
                self.state is ObjectState.PREV_SATURATED)

    def process_new_exposure(self, exposure):
        """Detects whether the tracked object is in the new exposure

        Assumes that the new exposure is the next one sequenitally in time
        from self.last_instance.exposure, and that this is only called on
        an active tracked object

        If there is a match will generate a new instance onto the end of the
        last exposure

        Internally modifies state but doesn't return anything
        """
        # NOTE(amelia): Should I check for active here?
        time_difference = exposure.date - self.last_instance.exposure.date

        x_new = self.last_instance.x + self.last_instance.vx * time_difference
        y_new = self.last_instance.y + self.last_instance.vy * time_difference

        key = (int(x_new/constants.PREDICT_RADIUS),
               int(y_new/constants.PREDICT_RADIUS))

        print("{}, {} ".format(self.last_instance.x, self.last_instance.y))

        print("{}, {} ".format(x_new, y_new))
        # check object is in exposure
        if (x_new < 0 or y_new < 0 or x_new >= exposure.xmax
                or y_new >= exposure.ymax):
            # TODO(amelia): Check when things are larger than the bounds
            # of the image to add to this
            self.state = ObjectState.END_OFF_FRAME
        elif key in exposure.predict_objects:
            match = exposure.predict_objects[key]
            x = match['xcentroid']
            y = match['ycentroid']
            mag = match['mag']

            if (abs(self.last_instance.mag - mag) >
                    constants.MAX_MAG_DIFFERENCE):
                self.state = ObjectState.LOST_MAGNITUDE_DIFFERENT
            else:
                # Compute new velocity
                vx = (x - self.last_instance.x)/time_difference
                vy = (y - self.last_instance.y)/time_difference
                new = ObjectFrameInstance(exposure, x, y, mag, vx, vy,
                                          x_new, y_new)

                # NOTE(amelia): validity checks ?
                # so because this is an object that is thought to be
                # already moving I don't have to do the same set of tests
                # as previously to check the new motion
                # we can be assured by the method that vx and vy will be
                # close to the previous value
                # ugh but over a large period that would also change
                # as such I would need to

                self.instances.append(new)
                self.state = ObjectState.ACTIVE

        elif exposure.mask[int(x_new), int(y_new)] is True:
            # the object is in a saturated part of the image, and
            # as such would not be picked up by the star finding algorithm
            # this will then try again another time

            # I want some good way to note that this instance exists
            # but that the object is saturated on it
            # like appending to instances but not setting it as last instance

            if self.state == ObjectState.PREV_SATURATED:
                self.state = ObjectState.LOST
            else:
                self.state = ObjectState.PREV_SATURATED
        else:
            self.state = ObjectState.LOST
            print("Object Lost")

    def print_stats(self):
        """ Print a summary of this tracking """
        # print length, time length, each of the instances
        pass

    def display(self):
        """ Display the full tracked trajectory across all images that
            the object appears in"""

        # TODO(amelia): check if this needs to have multiple layers of
        # subplots and how to do that
        # this is a decent way off however

        n = len(self.instances)
        fig, axs = plt.subplots(1, n, sharey='all', sharex='all')

        circle_rad = 3

        full_match, possible_matches = self.check_if_known()

        if full_match:
            print("Match across all images {}".format(full_match))
            fig.suptitle("Detected asteroids {}".format(full_match))
        else:
            print("No match across all images. Possible matches are:")
            print(possible_matches)

        colours = [plt.cm.gnuplot(i) for i in np.linspace(0, 1, 1.5 * n)]
        for i in range(n):
            frame = self.instances[i].exposure

            norm = ImageNormalize(frame.image, interval=ZScaleInterval())
            axs[i].imshow(frame.image, origin='lower', norm=norm)
            axs[i].set_title(frame.frame)

            for x in range(n):
                m = self.instances[x]

                # TODO(amelia): Pick better colour system for multiple things
                # TODO(amelia): Graph trajectory fully
                circle = Circle((m.x, m.y), circle_rad,
                                fill=False, edgecolor=colours[x],
                                linewidth=2)

                axs[i].add_patch(circle)

        plt.show()

    def save(self, folder=constants.OUTPUT_DIR):
        """ Save a record of this moving object

        The output file will have the name format of the first
        frame and the first detected x and y positions.

        Format of the output file is csv with columns frame, julian day,
        x, y, instrumental magnitude, vx, vy, predicted_x, predicted_y

        Note that x and y are relative to the image used to generate it
        (including cropping)
        """
        filename = folder + "/{}-{}-{}-{}.out".format(
            self.first_instance.exposure.frame,
            self.first_instance.exposure.subf,
            int(self.first_instance.x),
            int(self.first_instance.y))

        data = []
        for i in self.instances:
            data.append(",".join([str(x) for x in [
                i.exposure.filename, i.exposure.date, i.x, i.y, i.mag,
                i.vx, i.vy, i.predicted_x, i.predicted_y]]))
        data = "\n".join(data)

        with open(filename, 'w') as f:
            f.write(data)

    def check_if_known(self, all=True):
        """ Check if there are matching known positions of an asteroid
        for all of the instances

        Not yet implemented: hard=False
        Checks if there are matching for only some of the instances
        and report on all of those

        """

        possible_asteroids = None
        instance_asteroids = []

        for instance in self.instances:
            # what data structure
            instance_asteroids = instance.get_possible_asteroid_matches()
            if possible_asteroids is None:
                possible_asteroids = instance_asteroids
            else:
                for ast in possible_asteroids[:]:
                    if ast not in instance_asteroids:
                        possible_asteroids.remove(ast)
            instance_asteroids.append((instance, instance_asteroids))

        # Check if there is the same asteroid in all of these frames
        # or at least in the majority of them (TODO)
        if all:
            return possible_asteroids, instance_asteroids

        return possible_asteroids

    def __str__(self):
        return str(self.instances)

    def __repr__(self):
        return "<TrackedObject state={} instances={}>".format(
            self.state, self.instances)


class ObjectFrameInstance(object):
    """ Represents the instance of an object appearing on a single frame """

    def __init__(self, exposure, x, y, mag, vx=None, vy=None,
                 predicted_x=None, predicted_y=None):
        self.exposure = exposure
        self.x = x
        self.y = y
        self.mag = mag
        self.vx = vx
        self.vy = vy
        self.predicted_x = predicted_x
        self.predicted_y = predicted_y

        self.possible_matches = None
        self.possible_matches = self.get_possible_asteroid_matches()

    # TODO(amelia): decide if conversion to RA and DEC should be
    # handled here, I think that that will only be the case further
    # down the line when im clearer about how im handling things
    # and also making this more MOA specific

    def __eq__(self, other):
        # NOTE(amelia): velocity is also checked here as
        # it could be possible that two tracks pick up with a matching
        # object in between, this is unlikely (and it would be likely
        # that one of those was a suprious correlation), but like yknow

        return (self.exposure == other.exposure and self.x == other.x
                and self.y == other.y and self.vx == other.vx
                and self.vy == other.vy)

    def __str__(self):
        return "{} {} {}".format(self.exposure, self.x, self.y)

    def __repr__(self):
        return "<ObjectFrameInstance {} x={} y={}>".format(
            self.exposure, self.x, self.y)

    def get_possible_asteroid_matches(self):
        if self.possible_matches:
            return self.possible_matches

        x = self.x + self.exposure.subf[0]
        y = self.y + self.exposure.subf[1]
        # field, chip, jd, xccd, yccd
        return check_chip_field_for_asteroid(
            self.exposure.field, self.exposure.chip, self.exposure.date,
            x, y)


def check_for_new_objects(first, second, third):
    # NOTE(amelia): this code is just kinda ugly
    # I want to change it at some point but don't know how

    # time1 is time between first two exposures
    # time2 is time between second and third exposures

    time_1_2 = second.date - first.date
    time_2_pred = third.date - second.date

    matches = []
    for coord, row in first.objects.items():
        if check_near(second.objects, coord,
                      row['xcentroid'], row['ycentroid']):
            print("Non moving object")
            continue
        for coord2, row2 in list(second.objects.items()):
            if check_near(first.objects, coord2,
                          row2['xcentroid'], row2['ycentroid']):
                print("Non moving object (second frame)")
                del second.objects[coord2]
                # NOTE(amelia): not a super huge fan that this modifies the
                # object to its end
                continue

            if abs(row['mag'] - row2['mag']) > constants.MAX_MAG_DIFFERENCE:
                # check size is of same magnitudeish
                print("Magnitude too different")
                continue

            # compute distance change
            # compute velocity
            # compute new position
            # check if that matches image boundraries
            # place in new list/dict

            dx = row2['xcentroid'] - row['xcentroid']
            dy = row2['ycentroid'] - row['ycentroid']

            if dy**2 + dx**2 < constants.MATCH_RADIUS**2:
                print("Non moving object (predicted)")
                continue

            vx = dx/time_1_2
            vy = dy/time_1_2

            x_new = row2['xcentroid'] + vx * time_2_pred
            y_new = row2['ycentroid'] + vy * time_2_pred
            if (x_new < 0 or y_new < 0 or x_new >= third.xmax
                    or y_new >= third.ymax):
                # print("off frame")
                continue

            key = (int(x_new/constants.PREDICT_RADIUS),
                   int(y_new/constants.PREDICT_RADIUS))
            if key in third.predict_objects:
                # check this here
                # check that the position of the predicted object
                # is not in first and second
                match_object = third.predict_objects[key]
                if (abs(row['mag'] - match_object['mag'])
                        > constants.MAX_MAG_DIFFERENCE):
                    print("Position match, magnitude different")
                    continue

                match_coord = (int(x_new/constants.MATCH_RADIUS),
                               int(y_new/constants.MATCH_RADIUS))

                if check_near(first.objects, match_coord,
                              match_object['xcentroid'],
                              match_object['ycentroid']):
                    print("Non moving object (predicted)")
                    continue
                if check_near(second.objects, match_coord,
                              match_object['xcentroid'],
                              match_object['ycentroid']):
                    print("Non moving object (predicted)")
                    continue

                # tracked objects from rows?
                # or to instaces here first?
                first_instance = ObjectFrameInstance(
                    first, row['xcentroid'], row['ycentroid'], row['mag'])
                second_instance = ObjectFrameInstance(
                    second, row2['xcentroid'], row2['ycentroid'], row2['mag'],
                    vx, vy)

                dx = match_object['xcentroid'] - row2['xcentroid']
                dy = match_object['ycentroid'] - row2['ycentroid']

                vx_new = dx/time_2_pred
                vy_new = dy/time_2_pred

                third_instance = ObjectFrameInstance(
                    third, match_object['xcentroid'],
                    match_object['ycentroid'], match_object['mag'],
                    vx_new, vy_new, x_new, y_new)

                match = TrackedObject(
                    [first_instance, second_instance, third_instance])
                matches.append(match)
    return matches


def process_new_exposure(filename, exposures, active_objects,
                         old_objects, reference,
                         **kwargs):
    exp = Exposure(filename, reference=reference, **kwargs)

    print(len(exp.objects))

    if len(exposures) < 2:
        exposures.append(exp)
        return

    for object in active_objects[:]:
        object.process_new_exposure(exp)
        if not object.active:
            active_objects.remove(object)
            old_objects.append(object)

    new_objects = check_for_new_objects(exposures[-2], exposures[-1], exp)
    exposures.append(exp)

    # NOTE(amelia): this is O(n^2) however it is expected that there are
    # only a few asteroids on a given frame at a time so not a huge
    # performance issue
    for new_object in new_objects:
        # check if in active objects
        for object in active_objects:
            if object.last_instance == new_object.last_instance:
                print("Picked up same object, not starting new track "
                      "{}".format(object.last_instance))
                break
        else:
            active_objects.append(new_object)


if __name__ == '__main__':
    # Require imports from standard in to run detection

    # TODO(amelia): get subframe from sys.argv
    subf = (0, 0, 2048, 2048)

    if len(sys.argv) > 4:
        subf = (int(sys.argv[-4]), int(sys.argv[-3]),
                int(sys.argv[-2]), int(sys.argv[-1]))

    reference_filename = sys.stdin.readline().rstrip()

    exposure_files = [x.rstrip() for x in sys.stdin.readlines()
                      if x[0] != '#']

    # get reference image
    reference = Exposure(reference_filename, subf=subf)

    exposures = []
    active_objects = []
    old_objects = []

    for filename in exposure_files:
        process_new_exposure(filename, exposures, active_objects, old_objects,
                             reference, subf=subf)

    old_objects = old_objects + active_objects

    for match in old_objects:
        if match.long:
            # Check match for
            match.display()
            match.save(constants.OUTPUT_DIR)
