"""
Check if a single object is detected on a frame
Useful for checking why a known asteroid is not detected.
"""
import sys
from mp_detection.detect_moving_objects import Exposure
from matplotlib import pyplot as plt
from astropy.visualization.mpl_normalize import ImageNormalize
from astropy.visualization import ZScaleInterval
from matplotlib.patches import Circle, Rectangle


def display_exp_objects(exp, additional_points=[]):

    fig, axs = plt.subplots(1, 3, sharey='all', sharex='all')

    circle_rad = 3

    norm = ImageNormalize(exp.image, interval=ZScaleInterval())
    axs[0].imshow(exp.image, origin='lower', norm=norm)
    axs[0].set_title(exp.frame)

    axs[1].imshow(exp.mask, cmap='Greys', origin='lower')
    axs[1].set_title(exp.frame + " computed mask")

    norm = ImageNormalize(exp.reference.image, interval=ZScaleInterval())
    axs[2].imshow(exp.reference.image, origin='lower', norm=norm)
    axs[2].set_title(exp.frame + " reference")

    for _, ob in exp.objects.items():
        circle = Circle((ob['xcentroid'], ob['ycentroid']), circle_rad,
                        fill=False, edgecolor=(1, 0, 0), linewidth=1)
        axs[0].add_patch(circle)
        circle = Circle((ob['xcentroid'], ob['ycentroid']), circle_rad,
                        fill=False, edgecolor=(1, 0, 0), linewidth=1)

        axs[1].add_patch(circle)
        circle = Circle((ob['xcentroid'], ob['ycentroid']), circle_rad,
                        fill=False, edgecolor=(1, 0, 0), linewidth=1)

        axs[2].add_patch(circle)

    for x, y in additional_points:
        print(x, y)
        rect = Circle((x, y), circle_rad*2, linewidth=2,
                      edgecolor='b', facecolor='none')
        axs[0].add_patch(rect)
        rect = Circle((x, y), circle_rad*2, linewidth=2,
                      edgecolor='b', facecolor='none')
        axs[1].add_patch(rect)
        rect = Circle((x, y), circle_rad*2, linewidth=2,
                      edgecolor='b', facecolor='none')
        axs[2].add_patch(rect)

    plt.show()


if __name__ == '__main__':
    filename = sys.argv[-4]
    reference_filename = sys.argv[-3]
    x = float(sys.argv[-2])
    y = float(sys.argv[-1])

    # also get in there subframe ?????\
    subf = (0, 0, 2048, 2048)

    ref = Exposure(reference_filename, subf=subf)
    exp = Exposure(filename, reference=ref, subf=subf)
    display_exp_objects(exp, [(x, y)])
