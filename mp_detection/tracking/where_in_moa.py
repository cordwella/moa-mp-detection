#! /usr/bin/env python
#
import where

SUBFRAMES = where.subfxy.keys()


def byfield(field, ra, dec):
    for j in range(1, 11):
        chip = str(j)
        for subf in SUBFRAMES:
            par = where.get_par_byname(field, chip, subf)
            x, y = where.sky_to_ccd(par, ra, dec)
            x1, x2, y1, y2 = where.subfxy[subf]
            if x >= x1 and y >= y1 and x <= x2 and y <= y2:
                return chip, subf, x, y
    return '', '', -1, -1


def main(ra, dec):

    hits = []
    for i in range(1, 24):
        field = 'gb' + str(i)
        for j in range(1, 11):
            chip = str(j)
            for subf in SUBFRAMES:
                par = where.get_par_byname(field, chip, subf)
                x, y = where.sky_to_ccd(par, ra, dec)
                x1, x2, y1, y2 = where.subfxy[subf]
                if x >= x1 and y >= y1 and x <= x2 and y <= y2:
                    hits.append((field, chip, subf, x, y))
    return hits


if __name__ == '__main__':
    from geom import sex2dec

    instr = '''
    OGLE-2016-BLG-0674   1.07968574 -1.27769079  17:53:09.38   -28:40:02.7
    OGLE-2016-BLG-1110   1.09816572 -1.41223500  17:53:43.67   -28:43:11
    OGLE-2016-BLG-0559   1.66676558 -1.87577656  17:56:51.39   -28:27:44.5
    OGLE-2016-BLG-0594   1.44930736 -1.78487310  17:56:00.11   -28:36:17
    OGLE-2016-BLG-0613   1.98694226 -1.73757008  17:57:02.5    -28:06:58.2
    OGLE-2016-BLG-1058   2.43267266 -1.72021911  17:57:58.96   -27:43:18.2
    '''

    for line in instr.split('\n'):
        items = line.split()
        if len(items) < 5:
            continue
        name = items[0]
        s, y, b, n = name.split('-')
        shortname = s[0].lower() + b[0].lower() + y[2:] + n
        ras = items[3]
        decs = items[4]
        ra = 15*sex2dec(ras)
        dec = sex2dec(decs)
        hits = main(ra, dec)
        for field, chip, subf, x, y in hits:
            print(shortname, field, chip, x, y)
