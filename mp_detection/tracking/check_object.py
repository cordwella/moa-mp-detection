#! /usr/bin/env python3
#
'''
See if there is a known minor planet at a given position and time
'''
import sys

from tracking import mpconnect
from tracking import jdate
from tracking.where import find_cooridinates_from_chip_position
from tracking import geom


def check_for_asteriod(ra, dec, jd):

    year, month, fday = jdate.jd2cal(jd)
    res = mpconnect.mpcheck(str(year), str(month), fday, ra, dec, radius='5')
    asteroids = [int(x[1:].split(")")[0]) for x in res.split("\n")[3:-1]]
    return asteroids


def reformat_angle(ang):

    sex = geom.dec2sex(ang).lstrip('+')
    items = sex.split(':')
    sex = ' '.join(items)
    return sex


def check_chip_field_for_asteroid(field, chip, jd, xccd, yccd):
    ra, dec = find_cooridinates_from_chip_position(field, chip, xccd, yccd)
    ras = reformat_angle(ra / 15.0)
    decs = reformat_angle(dec)
    return check_for_asteriod(ras, decs, jd)


if __name__ == '__main__':

    field = sys.argv[1]
    chip = sys.argv[2]
    jd = float(sys.argv[3])
    xccd = float(sys.argv[4])
    yccd = float(sys.argv[5])
    ra, dec = find_cooridinates_from_chip_position(field, chip, xccd, yccd)
    ras = reformat_angle(ra / 15.0)
    decs = reformat_angle(dec)
    print("Conversion from field and chip to ra/dec in degrees and then in hours seconds minutes")
    print(field, chip, jd, xccd, yccd, '->', ra, dec, ras, decs)

    check_for_asteriod(ras, decs, jd)
