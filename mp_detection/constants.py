OUTPUT_DIR = "/home/amelia/Documents/summerresearch/asteroiddetection/output/"
IMAGE_OUTPUT_DIR = OUTPUT_DIR + "/images"
PARPATH = "/home/amelia/Documents/summerresearch/asteroiddetection/mp_detection/par"

MATCH_RADIUS = 2.5
MAX_POS_MISMATCH = 2
PREDICT_RADIUS = 10
MAX_MAG_DIFFERENCE = 2
MAX_MAG = 0

IMAGE_SATURATION_VALUE = 40000
MASK_RADIUS = 1

OUTPUT_DIR = "/home/amelia/Documents/summerresearch/asteroiddetection/output"
SAVE_REGISTERED_FILES = True
