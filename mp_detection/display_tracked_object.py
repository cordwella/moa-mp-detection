"""
For a tracked object with a tracked object file read it in and
display it.
"""

import sys
from mp_detection.detect_moving_objects import (
    Exposure, ObjectFrameInstance, TrackedObject)


def load_tracked_object(filename):
    subf = tuple(int(x) for x in
                 filename.split("(")[-1].split(")")[0].split(","))
    instances = []
    with open(filename) as fn:
        for line in fn:
            print(line.split(","))
            exp_filename, jd, x, y, mag, _, _, _, _ = line.split(",")
            exp = Exposure(exp_filename, subf=subf,
                           get_objects=False)
            instances.append(ObjectFrameInstance(
                exp, float(x), float(y), float(mag)))

    return TrackedObject(instances)


if __name__ == '__main__':
    filename = sys.argv[-1]
    obj = load_tracked_object(filename)
    obj.display()
