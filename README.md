# Minor Planet Detector

Please use PEP8 in your code (or like at least most of it)
(c) Amelia Cordwell <acor102@aucklanduni.ac.nz>
Python 3 tested on ubuntu 18.04.

## Purpose
The purpose of this library is for a given set of images from the MOA database
detect whether there is a moving object (likey a minor planet), and if so
to then to see if it can be identified with a known minor planet or not.


## Installation

Clone this repository to a folder of your choice.

Create a virtual enviroment and install the libraries in requirements.txt.
```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

Add this directory to your python path.
```
export PYTHONPATH="${PYTHONPATH}:$(pwd)"
```
Adding this directory to your python path and entering your virtualenv is
required each time you use these scripts.

The modify the filepaths in mp_detection/constants.py to your hearts content.

## Usage

The main detection script is mp_detection/detect_moving_objects.py.
It takes two types of input. The first is the set of values for the area of
the image to crop to in the order, xstart, ystart, xend, yend. The files to
use are passed in through standard input. The first line of standard input
should be the filename of the reference file, and the next lines are the
filenames of each of the images to process. Lines (with the exception
of the reference file) will be ignored if they start with a hash.

Example:
```
cat input_files/157_input.txt | python mp_detection/detect_moving_objects.py 0 0 1024 1024
```

157_input.txt looks as below:
```
/home/amelia/Documents/summerresearch/asteroiddetection/source/ref-gb3-R-2.fit
/home/amelia/Documents/summerresearch/asteroiddetection/source/B1814-gb3-R-2.fit
/home/amelia/Documents/summerresearch/asteroiddetection/source/B1819-gb3-R-2.fit
/home/amelia/Documents/summerresearch/asteroiddetection/source/B1824-gb3-R-2.fit
/home/amelia/Documents/summerresearch/asteroiddetection/source/B1832-gb3-R-2.fit
/home/amelia/Documents/summerresearch/asteroiddetection/source/B1861-gb3-R-2.fit
#/home/amelia/Documents/summerresearch/asteroiddetection/source/B1865-gb3-R-2.fit
```

Output files will be created in the output folder as specified in constants.py.
Two types of output files exist.

The first are created each time a new exposure is processed, these are cropped
and registered against the reference image. These have the naming convention
reg-{original filename}-{cropping coordinates}.fit.

The second are created when a moving object is found that is in more than 3
frames. These contain the details of the moving object through the frames.
The filename format is {framename of first pickup}-{cropping coordinate}-{
first x coordinate}-{first y coordinate}.out

The format of the output file is csv with columns frame, julian day,
x, y, instrumental magnitude, vx, vy, predicted_x, predicted_y

To display a found object use the display_tracked_object script.
```
python mp_detection/display_tracked_object.py  "output/B1814-gb3-R-2-(0, 0, 1024, 1024)-240-918.out"
```

### Checking for object detection

If you are unsure whether or not the lack of detection of an asteroid track is
due to poor linear motion matching or due to the asteroid not being picked up
on all of the frames you can use the check_object_detection.py

This will display the cropped, registered image, it's saturation mask and it's
reference frame.

All images will display all detected new objects, as well as circle a point
specified on the command line.

```
python mp_detection/check_object_detection.py source/B1809-gb3-R-2.fit source/ref-gb3-R-2.fit 246.9748 969.0140
```
